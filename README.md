# EXT.ns_basetheme

## [NITSAN] TYPO3 Base Template

The architecture of parent/child TYPO3 theme concept.

- TER: https://extensions.typo3.org/extension/ns_basetheme
- Composer: https://packagist.org/packages/nitsan/ns-basetheme
- Documentation: https://docs.t3planet.com/en/latest/ExtThemes/Index.html
- Issues: https://gitlab.com/sanjay_nitsan/ns_basetheme/-/issues
- Support: https://t3planet.com/support